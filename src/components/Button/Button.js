import React from 'react'
import PropTypes from 'prop-types'

const Button = (props) => (
    <button color={props.name} size={props.size}>
        {props.name}
    </button>
)

Button.propTypes = {
    color: PropTypes.string,
    size: PropTypes.string,
    name: PropTypes.string
}

export default Button
